import time
import random
from pykeyboard import PyKeyboard

txt = ["Аксиома: любые способности, любой талант (иногда это относится и к гениям) - это на 90% труд. ",
       "Вот формула успеха: 90% труда плюс 10% таланта. Будете действовать по данной формуле - достигнете успеха ",
       "не только в освоении быстрой скорости печати, но и во многих своих начинаниях.",
       " Помните слова Иммануила Канта (1724-1804), немецкого философа и учёного: ",
       '"Гений - это талант изобретения того, чему нельзя учить или научиться".'
       ]


keyboard = PyKeyboard()
time.sleep(5)
for line in txt:
    for s in line:
        keyboard.press_key(s)
        time.sleep(random.randrange(4, 7, 1)/10)
        keyboard.release_key(s)
    keyboard.press_key("'")
    time.sleep(random.randrange(4, 7, 1)/10)
    keyboard.release_key("'")
    time.sleep(1)

    keyboard.press_key(keyboard.backspace_key)
    time.sleep(random.randrange(4, 7, 1)/10)
    keyboard.release_key(keyboard.backspace_key)
    time.sleep(1)




